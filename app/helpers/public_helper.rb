module PublicHelper
	def resource_name
		:user
	end

	def resource
		session[:step1].present? ? @resource ||= User.new(session[:step1]) : @resource ||= User.new
	end

	def devise_mapping
		@devise_mapping ||= Devise.mappings[:user]
	end
end
