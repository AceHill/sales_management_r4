class InsentivesController < ApplicationController
	before_filter :authenticate_user! 

	def index
		@sales = User.where("position=?", SALES).order("name ASC").map { |e| [e.name,e.id] }
    @insentives = []
  end		

  def get_list
    delivery = ReportProspect.where("LOWER(name) = 'delivery' ").first.id rescue nil
    @insentives = Report.where("((MONTH(created_at) = ? AND YEAR(created_at) = ? AND report_prospect_id = #{delivery}) OR (MONTH(updated_at) = ? AND YEAR(updated_at) = ? AND report_prospect_id = #{delivery})) AND user_id = ?",params[:month], params[:year],params[:month], params[:year],params[:sales_id]) if delivery.present?
    @cond = ["(user_id = #{params[:sales_id]}  and (MONTH(created_at) = #{params[:month]} and YEAR(created_at) = #{params[:year]}  AND report_prospect_id = #{delivery} ) OR  (MONTH(updated_at) = #{params[:month]} AND YEAR(updated_at) = #{params[:year]} AND report_prospect_id = #{delivery}))"] if delivery.present?
    respond_to do |format|
      format.js
    end
  end 	
  
  def export  
    delivery = ReportProspect.where("LOWER(name) = 'delivery' ").first.id rescue nil 
    insentives = Report.where("((MONTH(created_at) = ? AND YEAR(created_at) = ? AND report_prospect_id = #{delivery}) OR (MONTH(updated_at) = ? AND YEAR(updated_at) = ? AND report_prospect_id = #{delivery})) AND user_id = ?",params[:period_month], params[:period_year],params[:period_month], params[:period_year],params[:sales_id]) if delivery.present?
    path = Report.generate_xls(insentives, params, current_user.id)
    send_file path, :filename => "Laporan_Insentive_Penjualan_Salesman_&_Sales_Counter_#{Time.now.strftime("%H_%M_%S")}.xls"
  end 
    
  def get_sales
    delivery = ReportProspect.where("LOWER(name) = 'delivery' ").first.id rescue nil
    user_ids = Report.select("user_id").where("(MONTH(created_at) = ? AND YEAR(created_at) = ? AND report_prospect_id = #{delivery}) OR (MONTH(updated_at) = ? AND YEAR(updated_at) = ? AND report_prospect_id = #{delivery})",params[:month], params[:year],params[:month], params[:year])
    user_ids = user_ids.map { |e| e.user_id }
    if current_user.is_position(ADMINISTRATOR)
      raise "ADMIN"
      @sales_new = user_ids.present? ? User.where("id IN (#{user_ids.join(',')})") .order("name ASC") : nil
    elsif current_user.is_position(SUPERVISOR)
      @sales_new = user_ids.present? ? User.where("id IN (#{user_ids.join(',')}) AND parent_id = #{current_user.id}") .order("name ASC") : nil
    end
  	respond_to do |format|
      format.js
    end
  end 
end
