class ReportsController < ApplicationController
  before_filter :authenticate_user!
  before_filter :get_all_data_for_combobox
  before_filter :titleize_customer, :only=>[:create,:update]
  before_filter :customer_params, :only=>[:create,:update]
  before_filter :product_params, :only=>[:create,:update]
  before_filter :report_params, :only=>[:create,:update]

  def index
    @perpage = 25
    @call = 0
    @report_prospects = ReportProspect.all
    @branch_managers = User.where("position = #{HEAD_BRANCH}").order("name ASC").map { |e| [e.name,e.id] }
    @workday = (Date.today - Report.startdate_period.to_date).to_i
    if params[:filter].present?
      @call = History.filtered(params,current_user)
      @all_reports = Report.filtered(params,current_user)
      @reports = @all_reports.paginate(:page =>(params[:page] || 1)).per_page(@perpage)
      @workday = ((params['filter']['to'].to_date rescue Date.today) - params['filter']['from'].to_date).to_i
    else
      if current_user.is_position(ADMINISTRATOR)
        @all_reports = Report.per_period
      elsif current_user.is_position(VISITOR)
        @all_reports = Report.per_period
      elsif current_user.is_position(HEAD_BRANCH)
        @all_reports = Report.per_period.where("user_id IN (
                                  SELECT sales.id 
                                  FROM users sales 
                                  WHERE sales.parent_id IN (
                                    SELECT supervisor.id 
                                    FROM users supervisor 
                                    WHERE supervisor.parent_id = #{current_user.id}
                                  ) 
                    )")
      elsif current_user.is_position(SUPERVISOR)
        @all_reports = Report.per_period.where("user_id IN (SELECT id FROM users WHERE parent_id=#{current_user.id})")
      elsif current_user.is_position(SALES)
        @all_reports = Report.per_period.where("user_id = #{current_user.id}")
      end
      @reports = @all_reports.paginate(:page =>(params[:page] || 1)).per_page(35)
    end
  end

  def new
    return redirect_to reports_path if !current_user.is_position(SALES)
    @target = ""
  end

  def show
    @target = "show"
    @report = Report.find(params[:id])
    @customer = @report.customer
    @vehicle = @report.product
  end

  def writer
    @user = User.find(params[:id])
  end

  def edit
    @target = params[:id]
    @report = Report.find(params[:id])
    @customer = @report.customer if params[:customer].blank?
    @vehicle = @report.product if params[:product].blank?
  end

  def create
    @target = ""
    if params[:customer_id].present?
      customer = Customer.find(params[:customer_id])
      customer.update_attributes(customer_params.merge({:user_id => current_user.id}))
    else
      customer = Customer.new(customer_params.merge({:user_id => current_user.id}))
    end
    vehicle = Product.new(product_params)
    report = Report.new(report_params)
    @customer = customer
    @vehicle = vehicle
    @report = report
    Report.transaction do
      if !customer.save
        @customer = customer
        flash[:error] = "Maaf, Terjadi kegagalan penyimpanan data Konsumen"
        return render :new
      end
      if !vehicle.save
        @vehicle = vehicle
        flash[:error] = "Maaf, Terjadi kegagalan penyimpanan data Kendaraan"
        return render :new
      end
      report = Report.new(report_params.merge({:user_id => current_user.id,:customer_id => customer.id, :product_id => vehicle.id, :updated_by => current_user.id}))
      if !report.save
        @report = report
        flash[:error] = "Maaf, Terjadi kegagalan penyimpanan data Laporan"
        return render :new
      end
        flash[:success] = "Laporan berhasil dibuat"
        redirect_to reports_path

    end
  end

  def update
    @customer = Customer.new(customer_params.merge({:user_id => current_user.id}))
    @vehicle = Product.new(product_params)
    @report = Report.new(report_params)
    @target = params[:id]
    customer = Customer.find(params[:customer_id])
    vehicle = Product.find(params[:vehicle_id])
    report = Report.find(params[:id])

    if params[:customer][:village].blank?
        flash[:error] = "Maaf, Kecamatan harus diisi"
        return redirect_to edit_report_path(params[:id])
    end

    if params[:vehicle][:manufactured_year].blank?
        flash[:error] = "Maaf, Tahun produksi harus diisi"
        return redirect_to edit_report_path(params[:id])
    end

    if params[:report][:payment_type_id].blank?
        flash[:error] = "Maaf, Cara pembayaran harus diisi"
        return redirect_to edit_report_path(params[:id])
    end

    if params[:report][:selling_price].blank?
        flash[:error] = "Maaf, Harga Jual harus diisi"
        return redirect_to edit_report_path(params[:id])
    end

    if ReportProspect.find(params[:report][:report_prospect_id]).name=='SPK'
      if @report.advance.blank?
        flash[:error] = "Maaf, Tanda Jadi harus diisi bila SPK"
        return redirect_to edit_report_path(params[:id])
      end
    end
    if ReportProspect.find(params[:report][:report_prospect_id]).is_final
      result = validate_all_form(params)
      if result=="customer-false"
        flash[:error] = "Maaf, Data Konsumen anda belum terisi semua. Silahkan isi semua"
      elsif result=="vehicle-false"
        flash[:error] = "Maaf, Data Kendaraan anda belum terisi semua. Silahkan isi semua"
      elsif result=="report-false"
        flash[:error] = "Maaf, Data Laporan anda belum terisi semua. Silahkan isi semua"
      end
      if result!="true"
        return redirect_to edit_report_path(params[:id])
      end
    end
    if current_user.id != report.user_id
      flash[:error] = "Maaf, Anda tidak memiliki hak akses untuk merubah data"
      return redirect_to edit_report_path(params[:id])
    end
    status = false
    Report.transaction do
      if customer.update_attributes(customer_params.merge({:user_id => current_user.id}))
        report.touch
      end
      if vehicle.update_attributes(product_params)
        report.touch
      end
      if report.update_attributes(report_params.merge({:updated_by => current_user.id}))
        status = true
      end
    end

    if status
      flash[:success] = "Laporan berhasil diperbaharui"
    else
      flash[:error] = "Maaf, Terjadi kesalahan penyimpanan Laporan"
    end
    return redirect_to edit_report_path(params[:id])

  end

  def destroy
    
  end

  def filter_choosed
    @option = params[:option]
    @option_select = []
    @option_choosed = params[:option_choosed]
    case params[:selected]
      when "religion_id"
        @option_select = RELIGIONS
      when "report_capacity_id"
        @option_select = ReportCapacity.order("name ASC").map { |e| [e.name,e.id] }
      when "report_prospect_id"
        @option_select = ReportProspect.order("name ASC").map { |e| [e.name,e.id] }
      when "bank_id"
        @option_select = Bank.order("full_name ASC").map { |e| [e.full_name,e.id] }
      when "license_color_id"
        @option_select = LICENSE_COLOR
      when "product_category_id"
        @option_select = ProductCategory.order("name ASC").map { |e| [e.name,e.id] }
      when "product_type_id"
        @option_select = ProductType.order("name ASC").map { |e| [e.name,e.id] }
      # additional
      when "user_name"
        @option_select = User.where("position = #{SALES}").order("name ASC").map { |e| [e.name,e.id] }
      when "sex"
        @option_select = [["Pria",1],["Wanita",0]]
      when "payment_type_id"
        @option_select = PaymentType.order("name ASC").map { |e| [e.name,e.id] }
    end
    respond_to do |format|
      format.js
    end
  end

  def get_customer
    customers = Customer.where("user_id = #{current_user.id} AND name LIKE '%#{params[:keyword]}%'").limit(params[:max_rows])
    render :json => {:customers => customers}
  end

  def filter_position
    @type = params[:type]
    @option_select = []
    @option_choosed = params[:selected]
    case params["type"]
      when "branch_manager"
        @option_select = User.where("position = '#{SUPERVISOR}' AND parent_id = #{params[:filter_id]}").order("name ASC").map { |e| [e.name, e.id] }
      when "supervisor"
        @option_select = User.where("position = '#{SALES}' AND parent_id = #{params[:filter_id]}").order("name ASC").map { |e| [e.name, e.id] }
      
    end if params[:filter_id].present?
    respond_to do |format|
      format.js
    end
  end

  def set_color
    report  = Report.find(params["id"])
    if report.update_attributes({color: params["color"],business_sector: " - ",updated_by: current_user.id})
      return render json: ({result: "success"})
    else
      return render json: ({result: report.errors})
    end
  end

  private
    def get_all_data_for_combobox
      @product_category = ProductCategory.order("name ASC")
      @product_type = ProductType.order("name ASC")
      @banks = Bank.order("full_name ASC")
      @payment_type = PaymentType.order("name ASC")
      @report_prospect = ReportProspect.order("name ASC")
      @report_capacity = ReportCapacity.order("name ASC")
      @payment_type = PaymentType.order("name ASC")
    end

    def validate_all_form(params)
      # validate customer
      params[:customer].each do |key,customer|
        return "customer-false" if customer.blank?
      end

      status = true
      params[:vehicle].each do |key,vehicle|
        return "vehicle-false" if vehicle.blank?
      end

      status = true
      params[:report].each do |key,report|
        return "report-false" if report.blank?
      end
      return "true"
      # c = params[:customer]
      # if  c[:company_name].present? && c[:name].present? && c[:address].present? && c[:sub_district].present? && c[:village].present? && c[:date_of_birth].present? && c[:district].present? && c[:city].present? && c[:sex].present? && c[:phone_number].present? && c[:handphone_number].present? && c[:other_phone_number].present? && c[:religion_id].present? && c[:bank_id].present?

      # end
      # return false unless status
    end

    def titleize_customer
      if params[:customer].present?
        params[:customer][:company_name] = params[:customer][:company_name].capitalize.titleize if params[:customer][:company_name].present?
        params[:customer][:company_name] = params[:customer][:company_name].capitalize.titleize if params[:customer][:company_name].present?
        params[:customer][:name] = params[:customer][:name].capitalize.titleize if params[:customer][:name].present?
        params[:customer][:sub_district] = params[:customer][:sub_district].capitalize.titleize if params[:customer][:sub_district].present?
        params[:customer][:village] = params[:customer][:village].capitalize.titleize if params[:customer][:village].present?
        params[:customer][:district] = params[:customer][:district].capitalize.titleize if params[:customer][:district].present?
        params[:customer][:city] = params[:customer][:city].capitalize.titleize if params[:customer][:city].present?
      end
      if params[:vehicle].present?
        params[:vehicle][:license_name] = params[:vehicle][:license_name].capitalize.titleize if params[:vehicle][:license_name].present?
        params[:vehicle][:license_number] = params[:vehicle][:license_number].upcase if params[:vehicle][:license_number].present?
        params[:vehicle][:chassis_number] = params[:vehicle][:chassis_number].upcase if params[:vehicle][:chassis_number].present?
        params[:vehicle][:engine_number] = params[:vehicle][:engine_number].upcase if params[:vehicle][:engine_number].present?
        params[:vehicle][:color] = params[:vehicle][:color].upcase if params[:vehicle][:color].present?
      end
      params[:report][:business_sector] = params[:report][:business_sector].upcase if params[:report][:business_sector].present? if params[:report].present?
    end

    def customer_params
      params.require(:customer).permit(:company_name, :name, :address, :sub_district, :village, :date_of_birth, :district, :city, :sex, :phone_number, :handphone_number, :other_phone_number, :religion_id, :bank_id)
    end

    def product_params
      params.require(:vehicle).permit(:license_name, :license_number, :manufactured_year, :chassis_number, :engine_number, :engine_capacity, :license_color_id, :product_category_id, :product_type_id, :color)
    end

    def report_params
      params.require(:report).permit(:business_sector,:bank_id,:product_type_id,:report_prospect_id,:report_capacity_id,:selling_price,:advance,:notes,:payment_type_id)
    end
end