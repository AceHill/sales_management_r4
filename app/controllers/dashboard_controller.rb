class DashboardController < ApplicationController
  before_filter :authenticate_user!

  # ivan.andhika.c@gmail.com
  # 17 Jan 2014
  # show index dashboard route: /dashboard
  def index
    @quote = Quote.where("is_active = TRUE").first rescue nil
    @news = News.where("expired_date >= CURDATE() ").paginate(:page =>(params[:page] || 1))
  end
end
