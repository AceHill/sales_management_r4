class HistoriesController < ApplicationController

  def show
    @histories = History.where(:report_id => params[:id]).paginate(:page =>(params[:page] || 1)).per_page(20)
    @parent_report = Report.find(params[:id])
  end

end
