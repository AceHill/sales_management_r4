class Configurations::PaymentTypesController < Configurations::ConfigurationsController
	before_filter :authenticate_user!
  before_filter :find_type , :only => [:edit, :update, :destroy]

	def index
    @payment_types = PaymentType.with_deleted.order('id ASC').order('deleted_at DESC').paginate(:page =>(params[:page] || 1))
  end
  	
  def new
    @payment_type = PaymentType.new
    @render_address = configurations_payment_types_path
  end 

  def create
    @payment_type = PaymentType.new(payment_params)
    if @payment_type.save(payment_params)
      flash[:notice] = "Selamat, Anda berhasil menambah cara pembayaran."
      redirect_to configurations_payment_types_path 
    else
      flash[:notice] = "Maaf, Anda gagal menambah cara pembayaran."
      p @payment_type.errors
      render :new
    end     
  end

  def show;end  

  def edit
    @render_address = configurations_payment_type_path
  end  

  def update    
    if @payment_type.update_attributes(payment_params)
      flash[:notice] = "Selamat, Anda berhasil mengupdate cara pembayaran."
      redirect_to configurations_payment_types_path
    else
      p @payment_type.errors
      flash[:notice] = "Maaf, Anda gagal mengupdate cara pembayaran."
      render :action =>  :edit
    end  
  end

  def update_is_active
    affectedRows =  PaymentType.update_all( "is_active = false", "id > 0" )
   
    payment_type = PaymentType.find(params[:id])    
    if payment_type.update_attributes(:is_active => true);
      render :text => "true"     
    else
      render :text => "false" 
    end
  end


  def destroy
    if @payment_type.destroy     
      flash[:notice] = "Selamat, Anda berhasil menghapus cara pembayaran."      
      render :text => "true"
    else
      flash[:error] = "Maaf, Anda gagal menghapus cara pembayaran."
      render :text => "false"
    end
  end  

  def recover
    record_to_recover = PaymentType.with_deleted.find(params["id"])
    if record_to_recover
      PaymentType.restore(params["id"])         
      render :text => "true"
    else
      render :text => "false"
    end
  end

  private  

  def find_type
    @payment_type = PaymentType.find(params[:id])
  end 

  def payment_params
    params.require(:payment_type).permit(:name)
  end	
    
end
