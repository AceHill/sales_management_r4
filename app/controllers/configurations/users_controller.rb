class Configurations::UsersController < Configurations::ConfigurationsController
  before_filter :authenticate_user!
  before_filter :find_user , :only => [:show,:edit, :update, :destroy]
  before_filter :set_gender , :only => [:edit, :update, :new, :create]

  def index
    @users = User.with_deleted.order('name ASC').order('deleted_at DESC').paginate(:page =>(params[:page] || 1))
  end
  	
  def new
    @user = User.new
  end 

  def create
    @user = User.new(user_params)
    if @user.save(user_params)
      flash[:notice] = "Selamat, Anda berhasil menambah karyawan."
      redirect_to configurations_users_path 
    else
      flash[:notice] = "Maaf, Anda gagal menambah karyawan."
      p @user.errors
      render :new
    end     
  end

  def show;end  

  def edit;end  

  def update    
    if @user.update_attributes(user_params)
      flash[:notice] = "Selamat, Anda berhasil mengupdate karyawan."
      redirect_to configurations_users_path
    else
      p @user.errors
      flash[:notice] = "Maaf, Anda gagal mengupdate karyawan."
      render :action =>  :edit
    end  
  end

  def destroy
    if @user.email != "admin@mtnsps.com"
      @user.destroy      
      flash[:notice] = "Selamat, Anda berhasil me-nonaktifkan karyawan."      
    else
      flash[:notice] = "Maaf, Anda gagal me-nonaktifkan karyawan."
    end
    
    redirect_to configurations_users_path
  end  
    
  def search
    @users = User.where("username or name LIKE '%#{params[:keyword]}%'").order("created_at asc")
    # raise @users.inspect
    respond_to do |format|
      format.js
    end
  end  

  private

  def set_gender
    @gender =  [["Pria", 1], ["Wanita" , 0]]
    @position =  [["Admin", ADMIN], ["Head Branch" , HEAD_BRANCH], ["Supervisor" , SUPERVISOR], ["Sales" , SALES], ["Visitor" , VISITOR], ["Developer" , DEVELOPER]]
  end  

  def find_user
    @user = User.find(params[:id])
    @gender =  [["Pria", 1], ["Wanita" , 0]]
  end 
    
  def user_params
    params.require(:user).permit(:username, :position,:email, :profile_picture ,:city, :name, :sex, :address, :phone_number, 
      :user_type_id, :password)
  end	
end
