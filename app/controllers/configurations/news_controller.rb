class Configurations::NewsController < Configurations::ConfigurationsController
	before_filter :authenticate_user!
  before_filter :find_news , :only => [:edit, :update, :destroy]

	def index
    @news = News.all.order('id ASC').paginate(:page =>(params[:page] || 1))
  end
  	
  def new
    @news = News.new
    @render_address = configurations_news_index_path
  end 

  def create
    @news = News.new(news_params)
    @render_address = configurations_news_index_path
    if @news.save(news_params)
      flash[:notice] = "Selamat, Anda berhasil menambah berita."
      redirect_to configurations_news_index_path 
    else
      flash[:notice] = "Maaf, Anda gagal menambah berita."
      p @news.errors.full_messages
      render :new
    end     
  end

  def edit
    @render_address = configurations_news_path
  end  

  def update    
    @render_address = configurations_news_path
    if @news.update_attributes(news_params)
      flash[:notice] = "Selamat, Anda berhasil mengupdate berita."
      redirect_to configurations_news_index_path
    else
      p @news.errors
      flash[:notice] = "Maaf, Anda gagal mengupdate berita."
      render :action =>  :edit
    end  
  end

  def destroy
    if @news.destroy          
      render :text => "true"
    else
      render :text => "false"
    end
  end  

  private  

  def news_params
    params.require(:news).permit(:title, :synopsis,:content, :news_image, :user_id, :expired_date)
  end 	

  def find_news
    @news = News.find(params[:id])
  end 
end
