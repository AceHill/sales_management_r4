class Configurations::OfficeHoursController < Configurations::ConfigurationsController
  before_filter :authenticate_user!
  before_filter :find_hour , :only => [:edit, :update, :destroy]

  def index
    @office_hours = OfficeHour.all.paginate(:page =>(params[:page] || 1))
  end
  	
  def new
    @office_hour = OfficeHour.new
    @office_hour.start_hour = Time.parse("00:00")
    @office_hour.end_hour = Time.parse("00:00")
    @render_address = configurations_office_hours_path
  end 

  def create
    @office_hour = OfficeHour.new(hour_params)
    @render_address = configurations_office_hours_path
    if @office_hour.save(hour_params)
      flash[:notice] = "Selamat, Anda berhasil menambah jam kerja."
      redirect_to configurations_office_hours_path 
    else
      flash[:notice] = "Maaf, Anda gagal menambah jam kerja."
      p @office_hour.errors
      render :new
    end     
  end

  def edit
    @render_address = configurations_office_hour_path
  end  

  def update    
    @render_address = configurations_office_hour_path
    if @office_hour.update_attributes(hour_params)
      flash[:notice] = "Selamat, Anda berhasil mengupdate jam kerja."
      redirect_to configurations_office_hours_path
    else
      p @office_hour.errors
      flash[:notice] = "Maaf, Anda gagal mengupdate jam kerja."
      render :action =>  :edit
    end  
  end

  def destroy
    if @office_hour.destroy          
      render :text => "true"
    else  
      render :text => "false"
    end
  end  

  private  

  def hour_params
    c = "#{params[:office_hour][:"start_hour(4i)"]}:#{ params[:office_hour][:"start_hour(5i)"]}"
    cto_date = c.to_time
    
    params[:office_hour][:start_hour] = cto_date #DateTime.strptime(cto_date,"%H:%M:%S")#c.strftime("%H:%M:00")
    
    c2 = "#{params[:office_hour][:"end_hour(4i)"]}:#{ params[:office_hour][:"end_hour(5i)"]}"
    params[:office_hour][:end_hour] = c2.to_time#DateTime.strptime(c2,"%H:%M:%S")#c.strftime("%H:%M:00")

    params.require(:office_hour).permit(:start_hour, :end_hour)
  end 	

  def find_hour
    @office_hour = OfficeHour.find(params[:id])
  end 
end

