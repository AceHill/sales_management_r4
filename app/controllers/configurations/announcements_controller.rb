class Configurations::AnnouncementsController < Configurations::ConfigurationsController
  before_filter :authenticate_user!
  
  def index
  	@announcement = Announcement.last
  end
 
  def update
  	if params[:id] != "0"
	    if Announcement.find(params[:id]).update_attributes(:announcement => params[:value])
	      render :text => "true"
	    else
	    	render :text => "false"
	    end 
	  else
	    Announcement.create(:announcement => params[:value])
	  	render :text => "true"
	  end  	
  end 	
end
