class Configurations::ReportCapacitiesController < Configurations::ConfigurationsController
  before_filter :authenticate_user!
  before_filter :find_capacity , :only => [:edit, :update, :destroy]

  def index
    @capacities = ReportCapacity.with_deleted.order('id ASC').order('deleted_at DESC').paginate(:page =>(params[:page] || 1))
  end
  	
  def new
    @capacity = ReportCapacity.new
    @render_address = configurations_report_capacities_path
  end 

  def create
    @capacity = ReportCapacity.new(capacity_params)
    @render_address = configurations_report_capacities_path
    if @capacity.save(capacity_params)
      flash[:notice] = "Selamat, Anda berhasil menambah status kapasitas."
      redirect_to configurations_report_capacities_path 
    else
      flash[:notice] = "Maaf, Anda gagal menambah status kapasitas."
      p @capacity.errors
      render :new
    end     
  end

  def edit
    @render_address = configurations_report_capacity_path
  end  

  def update    
    @render_address = configurations_report_capacity_path
    if @capacity.update_attributes(capacity_params)
      flash[:notice] = "Selamat, Anda berhasil mengupdate status kapasitas."
      redirect_to configurations_report_capacities_path
    else
      p @capacity.errors
      flash[:notice] = "Maaf, Anda gagal mengupdate status kapasitas."
      render :action =>  :edit
    end  
  end

  def destroy
    if @capacity.destroy          
      render :text => "true"
    else
      render :text => "false"
    end
  end  

  def recover
    record_to_recover = ReportCapacity.with_deleted.find(params["id"])
    if record_to_recover
      ReportCapacity.restore(params["id"])         
      render :text => "true"
    else
      render :text => "false"
    end
  end

  private  

  def capacity_params
    params.require(:report_capacity).permit(:name)
  end 	

  def find_capacity
    @capacity = ReportCapacity.find(params[:id])
  end 
end
