class Configurations::ProductCategoriesController < Configurations::ConfigurationsController
  before_filter :authenticate_user!
  before_filter :find_category , :only => [:edit, :update, :destroy]
  
  def index
    @categories = ProductCategory.with_deleted.order('id ASC').order('deleted_at DESC').paginate(:page =>(params[:page] || 1))
  end
  	
  def new
    @category = ProductCategory.new
    @render_address = configurations_product_categories_path
  end 

  def create
    @category = ProductCategory.new(category_params)
    @render_address = configurations_product_categories_path
    if @category.save(category_params)
      flash[:notice] = "Selamat, Anda berhasil menambah kategori."
      redirect_to configurations_product_categories_path 
    else
      flash[:notice] = "Maaf, Anda gagal menambah kategori."
      p @category.errors
      render :new
    end     
  end

  def edit
    @render_address = configurations_product_category_path
  end  

  def update    
    @render_address = configurations_product_category_path
    if @category.update_attributes(category_params)
      flash[:notice] = "Selamat, Anda berhasil mengupdate kategori."
      redirect_to configurations_product_categories_path
    else
      p @category.errors
      flash[:notice] = "Maaf, Anda gagal mengupdate kategori."
      render :action =>  :edit
    end  
  end

  def destroy
    if @category.destroy          
      render :text => "true"
    else
      render :text => "false"
    end
  end  

  def recover
    record_to_recover = ProductCategory.with_deleted.find(params["id"])
    if record_to_recover
      ProductCategory.restore(params["id"])         
      render :text => "true"
    else
      render :text => "false"
    end
  end

  private  

  def category_params
    params.require(:product_category).permit(:name)
  end 	

  def find_category
    @category = ProductCategory.find(params[:id])
  end 
end
