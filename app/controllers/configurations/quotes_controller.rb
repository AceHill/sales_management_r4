class Configurations::QuotesController < Configurations::ConfigurationsController
	before_filter :authenticate_user!
  before_filter :find_quote , :only => [:edit, :update, :destroy]

	def index
    @quotes = Quote.all.order('id ASC').paginate(:page =>(params[:page] || 1))
  end
  	
  def new
    @quote = Quote.new
    @render_address = configurations_quotes_path
  end 

  def create
    @quote = Quote.new(quote_params)
    if @quote.save(quote_params.merge(:is_active => 0))
      flash[:notice] = "Selamat, Anda berhasil menambah quote."
      redirect_to configurations_quotes_path 
    else
      flash[:notice] = "Maaf, Anda gagal menambah quote."
      p @quote.errors
      render :new
    end     
  end

  def show;end  

  def edit
    @render_address = configurations_quote_path
  end  

  def update    
    if @quote.update_attributes(quote_params)
      flash[:notice] = "Selamat, Anda berhasil mengupdate quote."
      redirect_to configurations_quotes_path
    else
      p @quote.errors
      flash[:notice] = "Maaf, Anda gagal mengupdate quote."
      render :action =>  :edit
    end  
  end

  def update_is_active
    affectedRows =  Quote.update_all( "is_active = false", "id > 0" )
   
    quote = Quote.find(params[:id])    
    if quote.update_attributes(:is_active => true);
      render :text => "true"     
    else
      render :text => "false" 
    end
  end


  def destroy
    if @quote.destroy     
      flash[:notice] = "Selamat, Anda berhasil menghapus quote."      
      render :text => "true"
    else
      flash[:error] = "Maaf, Anda gagal menghapus quote."
      render :text => "false"
    end
  end  

  private  

  def find_quote
    @quote = Quote.find(params[:id])
  end 

  def quote_params
    params.require(:quote).permit(:writer, :content).merge(:is_active=> 0)
  end	
    
end
