
class Configurations::ProductTypesController < Configurations::ConfigurationsController
before_filter :authenticate_user!
before_filter :find_product_type, :only => [:edit, :update, :destroy]

  def index
    @product_types = ProductType.with_deleted.order('id ASC').order('deleted_at DESC').paginate(:page =>(params[:page] || 1))
  end

  def new
    @product_type = ProductType.new
    @render_address = configurations_product_types_path
  end

  def create
    @product_type = ProductType.new(product_type_params)
    @render_address = configurations_product_types_path
    if @product_type.save(product_type_params)
      flash[:notice] = "Selamat, Anda berhasil menambah tipe kendaraan."
      redirect_to configurations_product_types_path 
    else
      flash[:notice] = "Maaf, Anda gagal menambah tipe kendaraan."
      p @product_type.errors
      render :new
    end     
  end

  def edit
    @render_address = configurations_product_type_path
  end  

  def update    
    @render_address = configurations_product_type_path
    if @product_type.update_attributes(product_type_params)
      flash[:notice] = "Selamat, Anda berhasil mengupdate tipe kendaraan."
      redirect_to configurations_product_types_path
    else
      p @product_type.errors
      flash[:notice] = "Maaf, Anda gagal mengupdate berita."
      render :action =>  :edit
    end  
  end

  def destroy
    if @product_type.destroy          
      render :text => "true"
    else
      render :text => "false"
    end
  end  

  def recover
    record_to_recover = ProductType.with_deleted.find(params["id"])
    if record_to_recover
      ProductType.restore(params["id"])         
      render :text => "true"
    else
      render :text => "false"
    end
  end


  private  

  def product_type_params
    params.require(:product_type).permit(:name)
  end 	

  def find_product_type
    @product_type = ProductType.find(params[:id])
  end 
  	
end
