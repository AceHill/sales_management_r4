class Configurations::ReportProspectsController < Configurations::ConfigurationsController
	before_filter :authenticate_user!
  before_filter :find_prospect , :only => [:edit, :update, :destroy]

	def index
    @prospects = ReportProspect.with_deleted.order('id ASC').order('deleted_at DESC').paginate(:page =>(params[:page] || 1))
  end
  	
  def new
    @prospect = ReportProspect.new
    @render_address = configurations_report_prospects_path
  end 

  def create
    @prospect = ReportProspect.new(prospect_params)
    @render_address = configurations_report_prospects_path
    if @prospect.save(prospect_params)
      flash[:notice] = "Selamat, Anda berhasil menambah status prospek."
      redirect_to configurations_report_prospects_path 
    else
      flash[:notice] = "Maaf, Anda gagal menambah status prospek."
      p @prospect.errors
      render :new
    end     
  end

  def edit
    @render_address = configurations_report_prospect_path
  end  

  def update    
    @render_address = configurations_report_capacity_path
    if @prospect.update_attributes(prospect_params)
      flash[:notice] = "Selamat, Anda berhasil mengupdate status prospek."
      redirect_to configurations_report_prospects_path
    else
      p @prospect.errors
      flash[:notice] = "Maaf, Anda gagal mengupdate status prospek."
      render :action =>  :edit
    end  
  end

  def destroy
    if @prospect.destroy          
      render :text => "true"
    else
      render :text => "false"
    end
  end  

  def recover
    record_to_recover = ReportProspect.with_deleted.find(params["id"])
    if record_to_recover
      ReportProspect.restore(params["id"])         
      render :text => "true"
    else
      render :text => "false"
    end
  end

  private  

  def prospect_params
    params[:report_prospect][:is_final] = false if params[:report_prospect][:is_final].nil?
    params.require(:report_prospect).permit(:name, :is_final)
  end 	

  def find_prospect
    @prospect = ReportProspect.find(params[:id])
  end 
end
