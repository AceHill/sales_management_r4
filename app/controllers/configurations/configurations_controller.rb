class Configurations::ConfigurationsController < ApplicationController

  protect_from_forgery
  before_filter :authenticate_user!, :is_admin?
  layout 'configurations'

  def require_admin_login
    # if current_user.nil? || !current_user.is_admin?
    #    flash[:error] = "Only admins are permitted"
    #    redirect_to log_in_path
    # else
    #    return current_user
    # end
  end

  def index
     render :layout => false
  end 	
end