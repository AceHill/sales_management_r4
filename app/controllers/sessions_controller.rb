class SessionsController < Devise::SessionsController
	

  def new
    self.resource = resource_class.new(sign_in_params)
    clean_up_passwords(resource)
    respond_with(resource, serialize_options(resource)) 

  end 
    
	def create
    self.resource = warden.authenticate!(auth_options)
    if resource.can_login?     
      flash[:notice] = "Anda berhasil login" if is_flashing_format?
      sign_in(resource_name, resource)
      yield resource if block_given?
      respond_with resource, :location => after_sign_in_path_for(resource)
    else      
      if resource.position == "#{SUPERVISOR}"
        flash[:error] = "Anda berada di luar jam kerja." 
      else 
        flash[:error] = "Supervisor Anda belum login atau anda berada di luar jam kerja." 
      end  
      sign_out(resource_name)
      respond_with resource, :location => new_user_session_path
    end  
  
  end
  
end
