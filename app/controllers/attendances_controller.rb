class AttendancesController < ApplicationController
	before_filter :authenticate_user!, :is_admin?

	def index
    @days_in_a_month = Time.now.end_of_month.day 
    @count_days = Time.now.day 
    @attendances = User.all.order("name ASC")
  end		

  def get_list
    # get count day in a month of Now:
  	@days_in_a_month = Time.now.end_of_month.day 
    filter_1 = DateTime.strptime(params[:start_date], "%m/%d/%Y")
    filter_2 = DateTime.strptime(params[:end_date], "%m/%d/%Y")
    # get last date after filter:
    @end_date = Time.days_in_month(filter_1.strftime("%m").to_i, filter_1.strftime("%y").to_i)
    # get first date after filter:
    @first_date = filter_1.strftime("%d").to_i    
    # count day after filter:
    @count_days = (filter_2 - filter_1).to_i 
  	@attendances = User.all

  	respond_to do |format|
      format.js
    end
  end	
end
