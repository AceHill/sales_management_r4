class CustomersController < ApplicationController
	before_filter :authenticate_user!, :is_admin?

	def index
    @customers = Customer.all.paginate(:page =>(params[:page] || 1)).per_page(35)
    @branch_managers = User.where("position = #{HEAD_BRANCH}").order("name ASC").map { |e| [e.name,e.id] }
    if params[:filter].present?
      @all_customers = Customer.filtered(params,current_user)
      @customers = @all_customers.paginate(:page =>(params[:page] || 1)).per_page(35)
    end
  end		

  def get_list
    @customers = Customer.all.paginate(:page =>(params[:page] || 1)).per_page(35)
  	respond_to do |format|
      format.js
    end
  end	


   def filter_choosed_customer
    @option = params[:option]
    @option_select = []
    @option_choosed = params[:option_choosed]
    case params[:selected]
      when "religion_id"
        @option_select = RELIGIONS
      when "report_capacity_id"
        @option_select = ReportCapacity.order("name ASC").map { |e| [e.name,e.id] }
      when "report_prospect_id"
        @option_select = ReportProspect.order("name ASC").map { |e| [e.name,e.id] }
      # additional
      when "user_name"
        @option_select = User.where("position = #{SALES}").order("name ASC").map { |e| [e.name,e.id] }
      when "sex"
        @option_select = [["Pria",1],["Wanita",0]]
    end
    respond_to do |format|
      format.js
    end
  end
end
