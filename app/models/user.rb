class User < ActiveRecord::Base
  acts_as_paranoid
  has_many :customers
  has_many :daily_reports
  has_many :histories
  has_many :news
  has_many :product
  has_many :reports

  has_many :sales, class_name: "User", foreign_key: "parent_id" , conditions: {:position => SALES}
  belongs_to :supervisor, class_name: "User", foreign_key: "parent_id" , conditions: {:position => SUPERVISOR}
  has_many :supervisors, class_name: "User", foreign_key: "parent_id" , conditions: {:position => SUPERVISOR}
  belongs_to :head_branch, class_name: "User", foreign_key: "parent_id" , conditions: {:position => HEAD_BRANCH}
  belongs_to :parent, class_name: "User", foreign_key: "parent_id"
  
  self.per_page = 30

  devise :database_authenticatable, :registerable, 
  :recoverable, :rememberable, :trackable, :timeoutable

  has_attached_file :profile_picture, :styles => { :medium => "300x300>", :thumb => "100x100>" }, :default_url => "/assets/personal-info.png",:preserve_files => true
  
  validates_presence_of :city,:phone_number,:username, :email, :address,
  :message=>"Required"
  validates_presence_of :password,  :message=>"Required", :on => :create
  validates_uniqueness_of :email,:message=>"Already in use"
  validate :check_gender

  def check_gender
    if self.nil?
      errors.add(:sex, "Sex Required.")
    end
  end

  def create_foot_prints
    s_sql = "SELECT `login_footprints`.* FROM `login_footprints` WHERE user_id = #{self.id} AND DATE(`login_footprints`.`login_time`) = '#{Time.now.to_date}'"
    existing_footprint = LoginFootprint.find_by_sql(s_sql)

    if existing_footprint.nil? || existing_footprint == [] 
      LoginFootprint.create({user_id: self.id, login_time: Time.now}) 
    end
  end

  def is_has_login_today
    spv_id = self.supervisor.id rescue nil
    unless spv_id.nil?
      s_sql = "SELECT `login_footprints`.* FROM `login_footprints` WHERE user_id = #{spv_id} AND DATE(`login_footprints`.`login_time`) = '#{Time.now.to_date}' "
      existing_footprint = LoginFootprint.find_by_sql(s_sql)

      return existing_footprint != []
    else
      return true
    end  
  end  

  def is_absent?(a_date, a_month, a_year)    
    dates = "#{a_year.to_s}/#{a_month.to_s}/#{a_date.to_s}"
    s_sql = "SELECT * FROM `daily_reports` WHERE user_id = #{self.id} AND DATE(`daily_reports`.`created_at`) = '#{dates}' "
    existing_attendances = DailyReport.find_by_sql(s_sql)
    if existing_attendances != []
      return "In"
    else
      return "-"
    end  
  end  

  def is_position(params_position)
    self.position == "#{params_position}" ? true : false
  end

  def get_position
    case self.position 
      when "1"
        return "Admin"
      when "2"
        return "Head Branch"
      when "3"
        return "Supervisor"    
      when "4"
        return "Sales"
      when "5"
        return "Visitor"  
      when "6"
        return "Developer"
      else
        return "-"  
      end  
  end  

  def is_valid_login_time
    count_boolean = 0
    is_valid =  OfficeHour.where("start_hour < ? and end_hour > ?",  "#{Time.now.strftime('%H:%M')}", "#{Time.now.strftime('%H:%M')}")
    return is_valid.size > 0
  end  

  def can_login?
    spv_of_login_user = self.supervisor
    #cek apakah supervisor sudah login hari ini
    if !spv_of_login_user.nil?
      if self.is_has_login_today         
        self.create_foot_prints       
        if self.is_valid_login_time
          return true
        else
          return false
        end        
      else
        return false
      end 
    else   
      if self.position == "#{SUPERVISOR}"
        if self.is_valid_login_time
          self.create_foot_prints
          return true
        else
          return false
        end 
      else
        self.create_foot_prints
        return true
      end  
    end      
  end 
end
