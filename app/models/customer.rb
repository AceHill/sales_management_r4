class Customer < ActiveRecord::Base
  has_many :reports
  belongs_to :user

  # :sub_district, :village, :district perlu di validate ga?
  validates_presence_of :name, :address, :city, :phone_number, :religion_id, :village ,:message => "Kolom tidak boleh kosong"
  # validates :sex, inclusion: { in: 0..1 } => "Kolom tidak boleh yang laen"

  def string_religion
    case self.religion_id.to_i
      when 0
        ISLAM[0]
      when 1
        CHRISTIANITY[0]
      when 2
        CATHOLIC[0]
      when 3
        BUDDHISM[0]
      when 4
        HINDUISM[0]
      when 5
        CONFUCIUS[0]
    end
  end

  def self.filtered(params, current_user)
    conditions = []
    # filter condition
    conditions << self.filter_condition(params[:filter][:option],params[:filter][:option_choosed]) if params[:filter][:option].present? && params[:filter][:option_choosed].present?

    self.where(conditions.join(" AND "))

  end

  private

    def self.filter_condition(option,option_choosed)
      case option
        when "religion_id"
          "religion_id = #{option_choosed}"
        when "user_name"
          "user_id = #{option_choosed}"
        when "created_at"
          "created_at = '#{option_choosed}' "
        when "updated_at"
          "updated_at = '#{option_choosed}' "
        when "company_name"
          "company_name LIKE '%#{option_choosed}%' "
        when "name"
          "name LIKE '%#{option_choosed}%' "
        when "address"
          "address LIKE '%#{option_choosed}%' "
        when "sex"
          "sex = #{option_choosed} "
        when "date_of_birth"
          "date_of_birth = '%#{option_choosed}%' "
        when "phone_number"
          "phone_number LIKE '%#{option_choosed}%' "
        when "handphone_number"
          "handphone_number LIKE '%#{option_choosed}%' "
        when "other_phone_number"
          "other_phone_number LIKE '%#{option_choosed}%' "
        when "sub_district"
          "sub_district LIKE '%#{option_choosed}%' "
        when "village"
          "village LIKE '%#{option_choosed}%' "
        when "district"
          "district LIKE '%#{option_choosed}%' "
        when "city"
          "city LIKE '%#{option_choosed}%' "
      end
    end
end

