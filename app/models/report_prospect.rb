class ReportProspect < ActiveRecord::Base
  has_many :reports
  acts_as_paranoid
  validates_presence_of :name ,:message=>"Required"
end
