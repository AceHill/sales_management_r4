class History < ActiveRecord::Base
  belongs_to :report
  belongs_to :user

  def self.per_period
    self.where("DATE(created_at) BETWEEN '#{Report.startdate_period}' AND '#{Report.enddate_period}'")
  end

  def self.filtered(params, current_user)
    reports = Report.filtered(params,current_user)
    arr_ids = reports.pluck(:id)
    arr_ids = arr_ids.join(", ")
    histories = History.where("report_id in (#{arr_ids})")
    histories.count
  end

end
