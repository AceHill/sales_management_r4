class News < ActiveRecord::Base
  belongs_to :user
  has_attached_file :news_image, :styles => { :medium => "300x300>", :thumb => "100x100>" }, :default_url => "/assets/image-coming-soon.png",:preserve_files => true

  validate :valid_expired_date
  validates_presence_of :title, :content, :expired_date, :synopsis ,:message=>"Required"

  def valid_expired_date
  	if !self.expired_date.blank?
  	  if self.expired_date <= Time.now
    	  errors.add(:expired_date, 'Cannot less than  today')
      end
    end
  end
end
