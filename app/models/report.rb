class Report < ActiveRecord::Base
  belongs_to :customer
  belongs_to :product
  belongs_to :report_capacity
  belongs_to :report_prospect
  belongs_to :bank
  belongs_to :user
  belongs_to :payment_type

  after_create :add_create_daily_report, :save_history
  after_update :add_update_daily_report, :save_history
  # :bank_id, :payment_type_id,
  validates_presence_of  :report_prospect_id, :report_capacity_id, :notes, :business_sector, :payment_type_id, :selling_price, :message => "Kolom tidak boleh kosong"

  def self.per_period
    self.where("DATE(created_at) BETWEEN '#{Report.startdate_period}' AND '#{Report.enddate_period}'")
  end

  def self.user_condition(params, current_user)
    sales_id = current_user.is_position(SALES) ? current_user.id : params[:filter][:sales_id]
    supervisor_id = current_user.is_position(SUPERVISOR) ? current_user.id : params[:filter][:supervisor_id]
    branch_manager_id = current_user.is_position(HEAD_BRANCH) ? current_user.id : params[:filter][:branch_manager_id]
    conditions = []
    if params[:filter][:sales_id].present? && (current_user.is_position(ADMIN) || current_user.is_position(VISITOR) || current_user.is_position(HEAD_BRANCH) || current_user.is_position(SUPERVISOR))
      conditions << "reports.user_id = #{sales_id}"
    elsif params[:filter][:supervisor_id].present? && (current_user.is_position(ADMIN) || current_user.is_position(VISITOR) || current_user.is_position(HEAD_BRANCH))
      conditions << "users.parent_id = #{supervisor_id}"
    elsif params[:filter][:branch_manager_id].present? && (current_user.is_position(ADMIN) || current_user.is_position(VISITOR))
      conditions << "users.id IN (
                                  SELECT sales.id 
                                  FROM users sales 
                                  WHERE sales.parent_id IN (
                                    SELECT supervisor.id 
                                    FROM users supervisor 
                                    WHERE supervisor.parent_id = #{branch_manager_id}
                                  ) 
                    )"
    else
      if current_user.is_position(HEAD_BRANCH)
        conditions << "users.id IN (
                                  SELECT sales.id 
                                  FROM users sales 
                                  WHERE sales.parent_id IN (
                                    SELECT supervisor.id 
                                    FROM users supervisor 
                                    WHERE supervisor.parent_id = #{branch_manager_id}
                                  ) 
                    )"
      elsif current_user.is_position(SUPERVISOR)
        conditions << "users.parent_id = #{supervisor_id}"
      elsif current_user.is_position(SALES)
        conditions << "reports.user_id = #{sales_id}"
      end
    end
    return conditions
  end

  def self.filtered(params, current_user)
    conditions = []
    # filter position
    conditions = Report.user_condition(params, current_user)

    # filter condition
    params[:filter][:from] = Date.parse(params[:filter][:from]).strftime("%Y-%m-%d") rescue nil
    params[:filter][:to] = Date.parse(params[:filter][:to]).strftime("%Y-%m-%d") rescue nil
    if params[:filter][:from].present? && params[:filter][:to].present?
      conditions << "DATE(reports.created_at) BETWEEN '#{params[:filter][:from]}' AND '#{params[:filter][:to]}'"
    elsif params[:filter][:from].present? && params[:filter][:to].nil? 
      conditions << "DATE(reports.created_at) BETWEEN '#{params[:filter][:from]}' AND '#{Report.enddate_period}'"
    elsif params[:filter][:from].nil? && params[:filter][:to].present?
      conditions << "DATE(reports.created_at) BETWEEN '#{Report.startdate_period}' AND '#{params[:filter][:to]}'"
    else
      conditions << "DATE(reports.created_at) BETWEEN '#{Report.startdate_period}' AND '#{Report.enddate_period}'"
    end

    conditions << self.filter_condition(params[:filter][:option],params[:filter][:option_choosed]) if params[:filter][:option].present? && params[:filter][:option_choosed].present?
    self.where(conditions.join(" AND ")).joins(:customer,:product,:user)
  end

  def self.startdate_period
    if Date.today.strftime("%d").to_i<=5
      Date.today.at_beginning_of_month.last_month.strftime("%Y-%m-")+"06"
    else
      Date.today.at_beginning_of_month.strftime("%Y-%m-")+"06"
    end
  end

  def self.enddate_period
    if Date.today.strftime("%d").to_i<=5
      Date.today.at_beginning_of_month.strftime("%Y-%m-")+"05"
    else
      Date.today.at_beginning_of_month.next_month.strftime("%Y-%m-")+"05"
    end
  end

  def self.statistic(report_prospect_id)
    self.where(:report_prospect_id => report_prospect_id).count
  end

  private

    def self.filter_condition(option,option_choosed)      
      case option
        when "religion_id"
          "customers.religion_id = #{option_choosed}"
        when "user_name"
          "reports.user_id = #{option_choosed}"
        when "created_at"
          "reports.created_at = '#{option_choosed}' "
        when "updated_at"
          "reports.updated_at = '#{option_choosed}' "
        when "report_capacity_id"
          "reports.report_capacity_id = #{option_choosed} "
        when "report_prospect_id"
          "reports.report_prospect_id = #{option_choosed} "
        when "advance"
          "reports.advance = '#{option_choosed}' "
        when "selling_price"
          "reports.selling_price = '#{option_choosed}' "
        when "bank_id"
          "reports.bank_id = #{option_choosed} "
        when "business_sector"
          "reports.business_sector LIKE '%#{option_choosed}%' "
        when "notes"
          "reports.notes LIKE '%#{option_choosed}%' "
        when "license_number"
          "products.license_number LIKE '%#{option_choosed}%' "
        when "license_name"
          "products.license_name LIKE '%#{option_choosed}%' "
        when "license_color_id"
          "products.license_color_id = #{option_choosed} "
        when "product_color"
          "products.color LIKE '%#{option_choosed}%' "
        when "product_category_id"
          "products.product_category_id = #{option_choosed} "
        when "product_type_id"
          "products.product_type_id = #{option_choosed} "
        when "manufactured_year"
          "products.manufactured_year LIKE '%#{option_choosed}%' "
        when "engine_capacity"
          "products.engine_capacity LIKE '%#{option_choosed}%' "
        when "engine_number"
          "products.engine_number LIKE '%#{option_choosed}%' "
        when "chassis_number"
          "products.chassis_number LIKE '%#{option_choosed}%' "
        when "payment_type_id"
          "reports.payment_type_id = #{option_choosed} "
        when "company_name"
          "customers.company_name LIKE '%#{option_choosed}%' "
        when "name"
          "customers.name LIKE '%#{option_choosed}%' "
        when "address"
          "customers.address LIKE '%#{option_choosed}%' "
        when "sex"
          "customers.sex = #{option_choosed} "
        when "date_of_birth"
          "customers.date_of_birth = '%#{option_choosed}%' "
        when "phone_number"
          "customers.phone_number LIKE '%#{option_choosed}%' "
        when "handphone_number"
          "customers.handphone_number LIKE '%#{option_choosed}%' "
        when "other_phone_number"
          "customers.other_phone_number LIKE '%#{option_choosed}%' "
        when "sub_district"
          "customers.sub_district LIKE '%#{option_choosed}%' "
        when "village"
          "customers.village LIKE '%#{option_choosed}%' "
        when "district"
          "customers.district LIKE '%#{option_choosed}%' "
        when "city"
          "customers.city LIKE '%#{option_choosed}%' "
      end
    end

    def add_create_daily_report
      if self.user_id == self.updated_by
        DailyReport.create({
          :user_id => self.user_id,
          :activity_id => CREATE,
          :report_id => self.id
          })
      end
    end

    def add_update_daily_report
      if self.user_id == self.updated_by
        DailyReport.create({
          :user_id => self.user_id,
          :activity_id => UPDATE,
          :report_id => self.id
          })
      end
    end

    def save_history
      History.create({:report_id => self.id, :user_id => self.updated_by, :changes_report => ({ customer: self.customer, report: self, vehicle: self.product }).to_json})
    end

    def self.generate_xls(insentives, params, printer_id)
      #update printed_at of each report
      insentives.each do |insentive|
        insentive.update_attributes(:printed_at => Time.now, :print_user_id => printer_id )
      end
      #--------------------------------
      format_title = Spreadsheet::Format.new(:weight => :bold,
                                            :text_wrap => true,
                                            :horizontal_align => :center)
      header0 = ["Laporan Insentive Penjualan Salesman & Sales Counter"]
      month_name = Date::MONTHNAMES[params[:period_month].to_i]
      header1 = ["Nama Salesman : #{User.find(params[:sales_id]).name rescue "-"}"]
      header2 = ["Periode: #{month_name} #{params[:period_year]}"]
      header3 = ["No", "Tgl SPK","Tgl Delivery","Nama Customer","Alamat Customer", "Jenis Produk", "Harga Jual", "No. Rangka", "No. Mesin", "Pembayaran Via"]

      book = Spreadsheet::Workbook.new
      nb_format = Spreadsheet::Format.new  :number_format => '#.00'
      sheet= book.create_worksheet :name => "Daftar Insentives"
      sheet.row(3).concat header3
      sheet.row(2).concat header2
      sheet.row(1).concat header1
      sheet.row(0).concat header0
      col_max = 8
      sheet.merge_cells(0, 0, 0, col_max)
      sheet.row(0).set_format(0,format_title)

      (0..9).each do |column|
        sheet.row(3).set_format(column, format_title)
        sheet.column(column).width = 20 if column != 0
      end
    
      last_row = 4
      insentives.each_with_index do |ins, index|
        last_row = last_row + 1
        payment_type_name = ins.payment_type.name rescue "-"
        customer_name = ins.customer.name rescue "-"
        customer_address = ins.customer.address rescue "-"
        # selling_price = number_to_currency(1234567890.50, unit: "Rp.", separator: ",", delimiter: "")
        row_array = [index+1,
                    (ins.updated_at.strftime("%d/%m/%y") rescue "-"),  
                    (ins.updated_at.strftime("%d/%m/%y") rescue "-"), 
                    customer_name,
                    customer_address,
                    ins.product.product_category.name,
                    ins.selling_price,
                    ins.product.chassis_number,
                    ins.product.engine_number,
                    payment_type_name]
        sheet.row(index+4).concat row_array
      end unless insentives.blank?



      # insentive1 = ["Insentive", "#{params[:unit1]} Unit", "Rp. #{params[:nominal1]}", "Rp. #{params[:total1]}", "", "Branch Manager", "#{params[:user1]} Unit", "Rp. #{params[:user_nominal_1]}", "Rp. #{params[:user_total_1]}"]
      # insentive2 = ["", "#{params[:unit2]} Unit", "Rp. #{params[:nominal2]}", "Rp. #{params[:total2]}", "", "Supervisor", "#{params[:user2]} Unit", "Rp. #{params[:user_nominal_2]}", "Rp. #{params[:user_total_2]}"]
      # insentive3 = ["", "#{params[:unit3]} Unit", "Rp. #{params[:nominal3]}", "Rp. #{params[:total3]}",  "", "Sales", "#{params[:user3]} Unit", "Rp. #{params[:user_nominal_3]}", "Rp. #{params[:user_total_3]}"]
      # insentive4 = ["", "#{params[:unit4]} Unit", "Rp. #{params[:nominal4]}", "Rp. #{params[:total4]}"]

      insentive1 = ["INSENTIVE", "Salesman OTS","", "UNIT X Rp.","", "`= Rp.", "", "", " ","", " ","", " "]
      insentive2 = ["", "Salesman /C","", "UNIT X Rp.","", "`= Rp.", "", "", " ","", " ","", " "]
      insentive3 = ["", "Bonus Laporan","", "UNIT X Rp.","", "`= Rp.",  "", "", " ","", " ","", " "]
      insentive4 = ["", "TOTAL","", "","", "`= Rp."]
      insentive5 = ["", " ","", " ","", " "]
      insentive6 = ["", "Supervisor","", "UNIT X Rp.","", "`= Rp."]
      insentive7 = ["", "Branch Manager","", "UNIT X Rp.","", "`= Rp."]

      sheet.row(last_row+1).concat insentive1
      sheet.row(last_row+2).concat insentive2
      sheet.row(last_row+3).concat insentive3
      sheet.row(last_row+4).concat insentive4
      sheet.row(last_row+5).concat insentive5
      sheet.row(last_row+6).concat insentive6
      sheet.row(last_row+7).concat insentive7
      
      last_row = last_row + 8
      username = User.find(params[:sales_id]).supervisor.name rescue "-"
      row_array1 = ["","","","","","","","Bandung , #{(Time.now).strftime('%d %B %Y')}"]
      row_array2 = ["","","","","","","","Mengetahui,"]
      row_array3 = ["","","","","","","","#{username}"]
      row_array4 = ["","","","","","","","Supervisor"]
      sheet.row(last_row+1).concat row_array1
      sheet.row(last_row+2).concat row_array2
      sheet.row(last_row+6).concat row_array3
      sheet.row(last_row+7).concat row_array4

      insentive1 = ["Insentives"]
      path = "#{Rails.root}/public/daftar_insentives_xls"
      FileUtils.mkdir_p path
      path << "/insentives_#{Time.now.strftime("%Y_%m_%d_%H_%M_%S")}.xls"
      book.write path
      return path
    end  
end
