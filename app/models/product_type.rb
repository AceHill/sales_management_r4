class ProductType < ActiveRecord::Base
  acts_as_paranoid
  has_many :products
  validates_presence_of :name ,:message=>"Required"
  
end
