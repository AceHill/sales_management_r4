class Product < ActiveRecord::Base
  belongs_to :product_category
  belongs_to :product_type
  belongs_to :user

   # :license_number, :manufactured_year, :chassis_number, :engine_number, :engine_capacity, 
  validates_presence_of :license_name, :product_category_id, :license_color_id, :product_type_id, :color, :manufactured_year, :message => "Kolom tidak boleh kosong"

  def string_license_color
    case self.license_color_id
      when 0
        BLACK[0]
      when 1
        YELLOW[0]
      when 2
        RED[0]
      when 3
        WHITE[0]
    end
  end
  
end
