class Bank < ActiveRecord::Base
  has_many :reports
  has_many :histories
end
