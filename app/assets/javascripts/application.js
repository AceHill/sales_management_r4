// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery-1.10.2
//= require jquery-ui-1.10.4.custom.min
//= require turbolinks
//= require bootstrap.min
//= require bootstrap.datepicker
	// knob
//= require jquery.knob
	// flot charts
//= require jquery.flot
//= require jquery.flot.stack
//= require jquery.flot.resize
//= require theme
//= require lapwing


arr_loc1 = document.URL.split('/');
arr_loc2 = arr_loc1[3].split('#');
arr_loc3 = arr_loc2[0].split('?');
current_locations = arr_loc3[0];
$(".sidebar-menu").each(function(index, el) {
$(el).removeClass('active');
$(el).children('.pointer').remove();
});
$('#sidebar-menu-'+current_locations).addClass('active');
$('#sidebar-menu-'+current_locations).append($('#hidden-pointer').html())
$(function() {
  $( ".datepicker" ).datepicker();
});

function export_to_xls(){
  // if($('#total_unit').val() == 0){
  //   alert("Perhitungan insentive tidak boleh 0");
  // }else 
  // if($('#total_unit').val() != 0){  
    window.location.href = '/insentives/export?period_month='+$('#date_month').val()+
      '&period_year='+$('#date_year').val()+
      '&sales_id='+$('#sales_name').val()+

      '&unit1='+$('#unit_1').val()+
      '&nominal1='+$('#nominal_1').val()+
      '&total1='+$('#total_1').val()+

      '&unit2='+$('#unit_2').val()+
      '&nominal2='+$('#nominal_2').val()+
      '&total2='+$('#total_2').val()+

      '&unit3='+$('#unit_3').val()+
      '&nominal3='+$('#nominal_3').val()+
      '&total3='+$('#total_3').val()+
      
      '&unit4='+$('#unit_4').val()+
      '&nominal4='+$('#nominal_4').val()+
      '&total4='+$('#total_4').val()+

      '&user1='+$('#user_1').val()+
      '&user_nominal_1='+$('#user_nominal_1').val()+
      '&user_total_1='+$('#user_total_1').val()+
      
      '&user2='+$('#user_2').val()+
      '&user_nominal_2='+$('#user_nominal_2').val()+
      '&user_total_2='+$('#user_total_2').val()+
      
      '&user3='+$('#user_3').val()+
      '&user_nominal_3='+$('#user_nominal_3').val()+
      '&user_total_3='+$('#user_total_3').val()
    // $.ajax({
    //   type: 'GET',
    //   url: '/insentives/export?period_month='+$('#date_month').val()+
    //   '&period_year='+$('#date_year').val()+
    //   '&sales_id='+$('#sales_name').val()+

    //   '&unit1='+$('#unit_1').val()+
    //   '&nominal1='+$('#nominal_1').val()+
    //   '&total1='+$('#total_1').val()+

    //   '&unit2='+$('#unit_2').val()+
    //   '&nominal2='+$('#nominal_2').val()+
    //   '&total2='+$('#total_2').val()+

    //   '&unit3='+$('#unit_3').val()+
    //   '&nominal3='+$('#nominal_3').val()+
    //   '&total3='+$('#total_3').val()+
      
    //   '&unit4='+$('#unit_4').val()+
    //   '&nominal4='+$('#nominal_4').val()+
    //   '&total4='+$('#total_4').val()+

    //   '&user1='+$('#user_1').val()+
    //   '&user_nominal_1='+$('#user_nominal_1').val()+
    //   '&user_total_1='+$('#user_total_1').val()+
      
    //   '&user2='+$('#user_2').val()+
    //   '&user_nominal_2='+$('#user_nominal_2').val()+
    //   '&user_total_2='+$('#user_total_2').val()+
      
    //   '&user3='+$('#user_3').val()+
    //   '&user_nominal_3='+$('#user_nominal_3').val()+
    //   '&user_total_3='+$('#user_total_3').val(),
    //   success: function(data){
    //     window.location = data.url;
    //   }
    // });
  // }  
}