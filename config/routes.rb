SalesManagementR4::Application.routes.draw do
  devise_for :users, controllers: {sessions: "sessions"}   

  root :to => redirect('/dashboard')
  match "/configurations" => "configurations/dashboard#index", via: [:get]

  resources :attendances do
    collection do
      get :get_list
    end
  end 
  resources :helps do
    collection do
    end
  end  

  resources :customers do
    collection do
      get :get_list
      get :filter_choosed_customer
      get :filter_position
    end
  end  

  resources :insentives do
    collection do
      get :get_list
      get :get_sales
      get :export
    end
  end  
  
  namespace :configurations do
    resources :users do
      collection do
        get :search
      end
    end  
    resources :dashboard
    resources :announcements
    resources :quotes do 
      collection do
        post :update_is_active
      end
    end 
    resources :news 
    # resources :business_sectors do
    #   member do
    #     get :recover
    #   end
    # end
    # resources :product_colors do
    #   member do
    #     get :recover
    #   end
    # end
    resources :product_types do
      member do
        get :recover
      end
    end  

    resources :product_categories do
      member do
        get :recover
      end
    end  
    
    resources :report_prospects  do
      member do
        get :recover
      end
    end  

    resources :report_capacities do
      member do
        get :recover
      end
    end  

    resources :payment_types do
      member do
        get :recover
      end
    end  

    resources :office_hours   
  end
  resources :dashboard
  resources :reports do
    collection do
      get :get_customer
      get :filter_choosed
      get :filter_position
      post :set_color
    end
    member do
      get :writer
    end
  end

  resources :histories do
    collection do
    end
    member do
    end
  end

  # devise_scope :users do
  #   get '/users/sign_out' => 'devise/sessions#destroy', :as => :destroy_user_session
  #   post '/users/sign_in' => 'devise/sessions#create', :as => :user_session
  # end

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
  
end
