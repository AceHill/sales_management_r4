# daily report
CREATE = 1
UPDATE = 2

# product
BLACK   = ["Hitam",0]
YELLOW  = ["Kuning",1]
RED     = ["Merah",2]
WHITE   = ["Putih",3]
LICENSE_COLOR = [BLACK,YELLOW,RED,WHITE]

# customer
ISLAM         = ["Islam",0]
CHRISTIANITY  = ["Kristen Protestan",1]
CATHOLIC      = ["Kristen Katholik",2]
BUDDHISM      = ["Budha",3]
HINDUISM      = ["Hindu",4]
CONFUCIUS     = ["Khonghucu",5]
RELIGIONS     = [ISLAM,CHRISTIANITY,CATHOLIC,BUDDHISM,HINDUISM,CONFUCIUS]

# color 
ROW_RED = "#FFBBBB"
ROW_GREEN = "#97C30A"
ROW_BLUE = "#1FBED6"
ROW_YELLOW = "#F7D708"

# filter
FILTER_OPTION = [
                  ["Semua","all"],
                  ["Penulis","user_name"],
                  ["Dibuat","created_at"],
                  ["Diperbaharui","updated_at"],
                  ["Status Kapasitas","report_capacity_id"],
                  ["Status Prospek","report_prospect_id"],
                  ["Uang Tanda Jadi","advance"],
                  ["Harga Jual","selling_price"],
                  ["Bank","bank_id"],
                  ["Sektor Bisnis","business_sector_id"],
                  ["Keterangan","notes"],
                  ["No STNK","license_number"],
                  ["Nama STNK","license_name"],
                  ["Warna Plat","license_color_id"],
                  ["Warna","product_color_id"],
                  ["Jenis","product_category_id"],
                  ["Tipe","product_type_id"],
                  ["Tahun","manufactured_year"],
                  ["Kapasitas","engine_capacity"],
                  ["No Mesin","engine_number"],
                  ["No Rangka","chassis_number"],
                  ["Nama Perusahaan","company_name"],
                  ["Nama Konsumen","name"],
                  ["Alamat","address"],
                  ["Jenis Kelamin","sex"],
                  ["Tanggal Lahir","date_of_birth"],
                  ["No. Telp 1","phone_number"],
                  ["No. Telp 2","handphone_number"],
                  ["No. Telp 3","other_phone_number"],
                  ["Kelurahan","sub_district"],
                  ["Kecamatan","village"],
                  ["Wilayah","district"],
                  ["Kota","city"],
                  ["Agama","religion_id"],
                  ["Cara Pembayaran","payment_type_id"]
                ]

# filter customer
FILTER_OPTION_CUST = [
                  ["Semua","all"],              
                  ["Dibuat","created_at"],
                  ["Diperbaharui","updated_at"],
                  ["Nama Perusahaan","company_name"],
                  ["Nama Konsumen","name"],
                  ["Alamat","address"],
                  ["Jenis Kelamin","sex"],
                  ["Tanggal Lahir","date_of_birth"],
                  ["No. Telp 1","phone_number"],
                  ["No. Telp 2","handphone_number"],
                  ["No. Telp 3","other_phone_number"],
                  ["Kelurahan","sub_district"],
                  ["Kecamatan","village"],
                  ["Wilayah","district"],
                  ["Kota","city"],
                  ["Agama","religion_id"]
                ]