class CustomFailure < Devise::FailureApp
   def redirect_url
      session[:failure]=true      
      new_user_session_path
   end
   
   def respond
     if http_auth?
       http_auth
     else
       redirect
     end
   end
 end