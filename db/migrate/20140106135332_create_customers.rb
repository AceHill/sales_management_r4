class CreateCustomers < ActiveRecord::Migration
  def change
    create_table :customers do |t|
      t.string  :company_name
      t.string  :name
      t.text    :address
      t.boolean :sex
      t.date    :date_of_birth
      t.string  :phone_number
      t.string  :handphone_number
      t.string  :other_phone_number
      t.string  :sub_district
      t.string  :village
      t.string  :discrict
      t.string  :city
      t.string  :religion_id #CONSTANT
      t.integer :user_id #relation
      t.timestamps
    end
  end
end
