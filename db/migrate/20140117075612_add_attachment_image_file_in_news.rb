class AddAttachmentImageFileInNews < ActiveRecord::Migration
  def self.up 
  	remove_column :news, :image_url
  	add_attachment :news, :news_image
  end

  def self.down
    add_column  :news, :image_url 
  	remove_attachment :news, :news_image
  end	
end
