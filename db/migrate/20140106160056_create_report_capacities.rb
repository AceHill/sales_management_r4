class CreateReportCapacities < ActiveRecord::Migration
  def change
    create_table :report_capacities do |t|
      t.string :name
      t.timestamps
    end
  end
end
