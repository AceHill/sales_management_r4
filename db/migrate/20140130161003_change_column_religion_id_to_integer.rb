class ChangeColumnReligionIdToInteger < ActiveRecord::Migration
  def change
  	change_column :customers, :religion_id, :integer
  end
end
