class CreateBanks < ActiveRecord::Migration
  def change
    create_table :banks do |t|
      t.string :full_name
      t.string :abbreviation
      t.timestamps
    end
  end
end
