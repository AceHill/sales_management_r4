class ChangeHistoriesTables < ActiveRecord::Migration
  def change
  	remove_column :histories, :report_capacity_id
  	remove_column :histories, :report_prospect_id
  	remove_column :histories, :notes
  	remove_column :histories, :advance
  	remove_column :histories, :selling_price
  	remove_column :histories, :business_sector_id
  	remove_column :histories, :bank_id
  	remove_column :histories, :customer_id
  	remove_column :histories, :product_id
  	add_column :histories, :changes_report, :text
  end
end
