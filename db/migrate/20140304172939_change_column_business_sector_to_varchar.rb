class ChangeColumnBusinessSectorToVarchar < ActiveRecord::Migration
  def change
  	change_column :reports, :business_sector, :string
  end
end
