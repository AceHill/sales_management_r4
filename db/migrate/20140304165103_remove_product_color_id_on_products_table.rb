class RemoveProductColorIdOnProductsTable < ActiveRecord::Migration
  def change
  	remove_column :products, :product_color_id
  end
end
