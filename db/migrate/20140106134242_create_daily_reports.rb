class CreateDailyReports < ActiveRecord::Migration
  def change
    create_table :daily_reports do |t|
      t.integer :user_id #relation
      t.integer :activity_id #CONSTANT
      t.integer :report_id # relation
      t.timestamps
    end
  end
end
