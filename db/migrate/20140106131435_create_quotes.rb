class CreateQuotes < ActiveRecord::Migration
  def change
    create_table :quotes do |t|
      t.string  :writer
      t.text    :content
      t.boolean :is_active
      t.timestamps
    end
  end
end
