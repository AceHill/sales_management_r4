class AddPrintedAtAndUserPrintIdInReport < ActiveRecord::Migration
  def change
  	add_column :reports, :printed_at, :datetime
  	add_column :reports, :print_user_id, :integer
  end
end
