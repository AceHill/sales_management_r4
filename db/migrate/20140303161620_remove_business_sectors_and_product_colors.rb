class RemoveBusinessSectorsAndProductColors < ActiveRecord::Migration
  def change
  	drop_table :business_sectors
  	drop_table :product_colors
  end
end
