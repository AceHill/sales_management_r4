class CreateReportProspects < ActiveRecord::Migration
  def change
    create_table :report_prospects do |t|
      t.string :name
      t.boolean :is_final
      t.timestamps
    end
  end
end
