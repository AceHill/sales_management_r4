class RenameColumnBusinessSectorIdOnReportsTable < ActiveRecord::Migration
  def change
  	rename_column :reports, :business_sector_id , :business_sector
  end
end
