class CreateLoginFootprints < ActiveRecord::Migration
  def change
    create_table :login_footprints do |t|
      t.integer :user_id
      t.datetime :login_time
      t.timestamps
    end
  end
end
