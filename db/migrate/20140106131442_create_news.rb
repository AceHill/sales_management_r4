class CreateNews < ActiveRecord::Migration
  def change
    create_table :news do |t|
      t.string    :title
      t.text      :synopsis
      t.text      :content
      t.text      :image_url
      t.integer   :user_id #relation
      t.datetime  :expired_date
      t.timestamps
    end
  end
end
