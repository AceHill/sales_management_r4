class CreatePaymentTypes < ActiveRecord::Migration
  def change
    create_table :payment_types do |t|
      t.string :name
      t.datetime :deleted_at
      t.timestamps
    end
  end
end
