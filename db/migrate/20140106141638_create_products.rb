class CreateProducts < ActiveRecord::Migration
  def change
    create_table :products do |t|
      t.string  :license_number
      t.string  :license_name
      t.integer :license_color_id #CONSTANT
      t.integer :product_color_id #relation
      t.integer :product_category_id #relation
      t.integer :product_type_id #relation
      t.integer :manufactured_year
      t.decimal :engine_capacity
      t.string  :engine_number
      t.string  :chassis_number
      t.integer :user_id #relation
      t.timestamps
    end
  end
end