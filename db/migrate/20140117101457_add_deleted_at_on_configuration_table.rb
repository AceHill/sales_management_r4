class AddDeletedAtOnConfigurationTable < ActiveRecord::Migration
  add_column :product_types, :deleted_at, :datetime
  add_column :product_categories, :deleted_at, :datetime
  add_column :report_prospects , :deleted_at, :datetime
  add_column :report_capacities , :deleted_at, :datetime
  add_column :banks , :deleted_at, :datetime
  add_column :business_sectors , :deleted_at, :datetime
  add_column :customers, :deleted_at, :datetime
  add_column :products, :deleted_at, :datetime
  add_column :product_colors, :deleted_at, :datetime
  add_column :reports , :deleted_at, :datetime
end
