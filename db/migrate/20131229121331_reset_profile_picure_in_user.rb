class ResetProfilePicureInUser < ActiveRecord::Migration
  def self.up 
  	remove_column :users, :profile_picture
  	add_attachment :users, :profile_picture
  end

  def self.down
    add_column  :users, :profile_picture 
  	remove_attachment :users, :profile_picture
  end	
end
