class CreateOfficeHours < ActiveRecord::Migration
  def change
    create_table :office_hours do |t|
      t.time :start_hour
      t.time :end_hour
      t.datetime :deleted_at
      t.timestamps
    end
  end
end
