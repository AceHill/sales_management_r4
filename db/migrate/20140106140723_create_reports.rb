class CreateReports < ActiveRecord::Migration
  def change
    create_table :reports do |t|
      t.integer :customer_id #relation
      t.integer :product_id #relation
      t.integer :report_capacity_id #relation
      t.integer :report_prospect_id #relation
      t.text    :notes
      t.decimal :advance
      t.decimal :selling_price
      t.integer :business_sector_id #relation
      t.integer :bank_id #relation
      t.integer :user_id #relation
      t.timestamps
    end
  end
end
