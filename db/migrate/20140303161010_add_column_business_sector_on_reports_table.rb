class AddColumnBusinessSectorOnReportsTable < ActiveRecord::Migration
  def change
  	add_column :reports, :business_sector, :string
  end
end
