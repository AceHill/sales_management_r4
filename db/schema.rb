# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20140612135700) do

  create_table "announcements", force: true do |t|
    t.text     "announcement"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "banks", force: true do |t|
    t.string   "full_name"
    t.string   "abbreviation"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.datetime "deleted_at"
  end

  create_table "customers", force: true do |t|
    t.string   "company_name"
    t.string   "name"
    t.text     "address"
    t.boolean  "sex"
    t.date     "date_of_birth"
    t.string   "phone_number"
    t.string   "handphone_number"
    t.string   "other_phone_number"
    t.string   "sub_district"
    t.string   "village"
    t.string   "district"
    t.string   "city"
    t.integer  "religion_id"
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.datetime "deleted_at"
  end

  create_table "daily_reports", force: true do |t|
    t.integer  "user_id"
    t.integer  "activity_id"
    t.integer  "report_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "histories", force: true do |t|
    t.integer  "report_id"
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text     "changes_report"
  end

  create_table "login_footprints", force: true do |t|
    t.integer  "user_id"
    t.datetime "login_time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "news", force: true do |t|
    t.string   "title"
    t.text     "synopsis"
    t.text     "content"
    t.integer  "user_id"
    t.datetime "expired_date"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "news_image_file_name"
    t.string   "news_image_content_type"
    t.integer  "news_image_file_size"
    t.datetime "news_image_updated_at"
  end

  create_table "office_hours", force: true do |t|
    t.time     "start_hour"
    t.time     "end_hour"
    t.datetime "deleted_at"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "payment_types", force: true do |t|
    t.string   "name"
    t.datetime "deleted_at"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "product_categories", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.datetime "deleted_at"
  end

  create_table "product_types", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.datetime "deleted_at"
  end

  create_table "products", force: true do |t|
    t.string   "license_number"
    t.string   "license_name"
    t.integer  "license_color_id"
    t.integer  "product_category_id"
    t.integer  "product_type_id"
    t.integer  "manufactured_year"
    t.decimal  "engine_capacity",     precision: 10, scale: 0
    t.string   "engine_number"
    t.string   "chassis_number"
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.datetime "deleted_at"
    t.string   "color"
  end

  create_table "quotes", force: true do |t|
    t.string   "writer"
    t.text     "content"
    t.boolean  "is_active"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "report_capacities", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.datetime "deleted_at"
  end

  create_table "report_prospects", force: true do |t|
    t.string   "name"
    t.boolean  "is_final"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.datetime "deleted_at"
  end

  create_table "reports", force: true do |t|
    t.integer  "customer_id"
    t.integer  "product_id"
    t.integer  "report_capacity_id"
    t.integer  "report_prospect_id"
    t.text     "notes"
    t.decimal  "advance",            precision: 10, scale: 0
    t.decimal  "selling_price",      precision: 10, scale: 0
    t.string   "business_sector"
    t.integer  "bank_id"
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.datetime "deleted_at"
    t.integer  "payment_type_id"
    t.datetime "printed_at"
    t.integer  "print_user_id"
    t.string   "color"
    t.integer  "updated_by"
  end

  create_table "users", force: true do |t|
    t.string   "email",                        default: "", null: false
    t.string   "encrypted_password",           default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",                default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.datetime "locked_at"
    t.string   "username"
    t.string   "confirmation_code"
    t.string   "name"
    t.boolean  "sex"
    t.string   "address"
    t.string   "city"
    t.string   "phone_number"
    t.integer  "user_type_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "profile_picture_file_name"
    t.string   "profile_picture_content_type"
    t.integer  "profile_picture_file_size"
    t.datetime "profile_picture_updated_at"
    t.datetime "deleted_at"
    t.string   "position"
    t.integer  "parent_id"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

end
