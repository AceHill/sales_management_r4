# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

users = User.create([
                {username: 'administrator', email: 'admin@mtnsps.com', confirmed_at: Time.now, password: 'administrator',  name: 'Mulyono Tantosa',  sex: true , address: 'Jl Lorem Ipsum 55', city: 'Bandung', phone_number: '0800 00 0000 00',user_type_id: 1 , position: ADMINISTRATOR},
                {username: 'headbranch', email: 'headbranch@mtnsps.com', confirmed_at: Time.now, password: 'headbranch',  name: 'Juanda Triadi',  sex: true , address: 'Jl Lorem Ipsum 44', city: 'Bandung', phone_number: '0800 99 9999 00',user_type_id: 1, position: HEAD_BRANCH },
                {username: 'supervisor', email: 'supervisor@mtnsps.com', confirmed_at: Time.now, password: 'supervisor',  name: 'Nani Triana',  sex: false , address: 'Jl Lorem Ipsum 33', city: 'Bandung', phone_number: '0800 22 9999 00',user_type_id: 1, position: SUPERVISOR
                 },
                {username: 'supervisor2', email: 'supervisor2@mtnsps.com', confirmed_at: Time.now, password: 'supervisor2',  name: 'Dina Triana',  sex: false , address: 'Jl Lorem Ipsum 33', city: 'Bandung', phone_number: '0800 72 9999 00',user_type_id: 1, position: SUPERVISOR
                 },
                {username: 'sales', email: 'sales@mtnsps.com', confirmed_at: Time.now, password: 'sales',  name: 'Junaedi',  sex: true , address: 'Jl Lorem Ipsum 22', city: 'Bandung', phone_number: '0800 98 2999 00',user_type_id: 1 , position: SALES, parent_id: 3},
                {username: 'sales2', email: 'sales2@mtnsps.com', confirmed_at: Time.now, password: 'sales2',  name: 'Anita',  sex: true , address: 'Jl Lorem Ipsum 22', city: 'Bandung', phone_number: '0800 98 1999 00',user_type_id: 1 , position: SALES, parent_id: 4},
                {username: 'visitor', email: 'visitor@mtnsps.com', confirmed_at: Time.now, password: 'visitor',  name: 'Rudiandri',  sex: true , address: 'Jl Lorem Ipsum 11', city: 'Bandung', phone_number: '0800 99 9999 87',user_type_id: 1 , position: VISITOR},
                {username: 'ivan', email: 'developer@mtnsps.com', confirmed_at: Time.now, password: 'mtnsps123',  name: 'Ivan Andhika',  sex: true , address: 'Jl Lorem Ipsum 10', city: 'Bandung', phone_number: '0800 77 9999 00',user_type_id: 1, position: DEVELOPER }
              ])


banks = Bank.create([
                    {abbreviation: 'BRI', full_name: 'Bank Rakyat Indonesia'}, 
                    {abbreviation: 'BCA', full_name: 'Bank Central Asia'},
                    {abbreviation: 'BNI', full_name: 'Bank Negara Indonesia'},
                    {abbreviation: 'BTN', full_name: 'Bank Tabungan Negara'},
                  ])

capacity_status = ReportCapacity.create([
                    {name: 'Potensial'}, 
                    {name: 'S. Potensial'}, 
                    {name: 'N. Potensial'}
                  ])
prospect_status = ReportProspect.create([
                    {name: 'LP', is_final: true}, 
                    {name: 'HP' ,is_final: false},
                    {name: 'SPK' , is_final: false}, 
                    {name: 'Delivery' , is_final: false},
                    {name: 'Los.Order' , is_final: false},
                    {name: 'Pending' , is_final: false} ,
                    {name: 'Batal' , is_final: false},
                    {name: 'N. Prospek', is_final: false}
                  ])


# product_colors = ProductColor.create([
#                     {name: 'Red'}, 
#                     {name: 'Blue'}, 
#                     {name: 'Black'},
#                     {name: 'Purple'}, 
#                     {name: 'Green'}, 
#                     {name: 'Yellow'}
#                   ])

# FOR DEMO ONLY

customers = Customer.create([
              { company_name: "Ace Hill",
                name: "Ivan Andhika",
                address: "Gg. Sereh No. 21",
                sex: 1,
                date_of_birth: "1990-05-08",
                phone_number: "0226036153",
                handphone_number: "08987126770",
                other_phone_number: "82126020101",
                sub_district: "Astana Anyar",
                village: "Bojongloa",
                district: "Karees",
                city: "Bandung",
                religion_id: 2,
                user_id: 1},
               { company_name: "Ace Hill development",
                name: "Visca Veronika",
                address: "Jl. Cikutra",
                sex: 0,
                date_of_birth: "1970-01-01",
                phone_number: "02291919911",
                handphone_number: "0898777777",
                other_phone_number: "821233331",
                sub_district: "Astana Anyar",
                village: "Bojongloa",
                district: "Karees",
                city: "Bandung",
                religion_id: 2,
                user_id: 1} 
              ])

quotes = Quote.create([
          {writer: "Napoleon Hill", content: "Whatever the mind of man can conceive and believe, it can achieve.", is_active: false},
          {writer: "Albert Einstein", content: "Strive not to be a success, but rather to be of value.", is_active: true},
          {writer: "Robert Frost", content: "Two roads diverged in a wood, and I took the one less traveled by, and that has made all the difference.", is_active: false}
        ])

products = Product.create([{ 
  license_number: "D 5033 GD", 
  license_name: "Ivan Andhika", 
  license_color_id: 1, 
  product_category_id: 1,
  product_type_id: 1, 
  manufactured_year: 2008, 
  engine_capacity: 1500, 
  engine_number: "MKMGMRJG23582", 
  chassis_number: "RVKDJ234232", 
  color: "Hitam",
  user_id: 1
},
{ 
  license_number: "D 3333 GD", 
  license_name: "Raisa", 
  license_color_id: 1, 
  product_category_id: 1,
  product_type_id: 1, 
  manufactured_year: 2008, 
  engine_capacity: 1500, 
  engine_number: "MKMGMRJG23582", 
  chassis_number: "RVKDJ234232", 
  color: "Putih",
  user_id: 2
},
{ 
  license_number: "D 1234 SD", 
  license_name: "Bianca", 
  license_color_id: 1, 
  product_category_id: 1,
  product_type_id: 1, 
  manufactured_year: 2009, 
  engine_capacity: 1500, 
  engine_number: "DKMGMRJG23582", 
  chassis_number: "DVKDJ234232", 
  color: "Putih",
  user_id: 1
},
{ 
  license_number: "D 9034 WD", 
  license_name: "Deassy B", 
  license_color_id: 1, 
  product_category_id: 1,
  product_type_id: 1, 
  manufactured_year: 2009, 
  engine_capacity: 1500, 
  engine_number: "DKMGMRJG23582", 
  chassis_number: "DVKDJ234232", 
  color: "Hitam",
  user_id: 2
},
{ 
  license_number: "D 1933 CD", 
  license_name: "Siti AMinah", 
  license_color_id: 2, 
  product_category_id: 1,
  product_type_id: 2, 
  manufactured_year: 2010, 
  engine_capacity: 1500, 
  engine_number: "K0MGMRJG23582", 
  chassis_number: "HYKDJ234232", 
  color: "Merah",
  user_id: 1
},
{ 
  license_number: "D 888 AI", 
  license_name: "Richard", 
  license_color_id: 1, 
  product_category_id: 1,
  product_type_id: 1, 
  manufactured_year: 2008, 
  engine_capacity: 1500, 
  engine_number: "S5KMGMRJG23582", 
  chassis_number: "RVKDJ234232", 
  color: "Kuning",
  user_id: 2
},
{ 
  license_number: "D 1111 MD", 
  license_name: "Ricka", 
  license_color_id: 3, 
  product_category_id: 1,
  product_type_id: 1, 
  manufactured_year: 2008, 
  engine_capacity: 1500, 
  engine_number: "D3KMGMRJG23582", 
  chassis_number: "D0DVKDJ234232", 
  color: "Kuning",
  user_id: 1
},
{ 
  license_number: "D 9034 SS", 
  license_name: "Deassy B", 
  license_color_id: 1, 
  product_category_id: 1,
  product_type_id: 1, 
  manufactured_year: 2009, 
  engine_capacity: 1500, 
  engine_number: "DKMGMRJG23582", 
  chassis_number: "DVKDJ234232", 
  color: "Hitam",
  user_id: 2
}])

# business_sectors = BusinessSector.create([
#                     {name: 'Niaga'}, 
#                     {name: 'Perbankan'}, 
#                     {name: 'Importir'},
#                     {name: 'Exportir'},
#                     {name: 'Bengkel'}
#                   ])


reports = Report.create([
          {customer_id: 1, product_id: 1, report_capacity_id: 1, report_prospect_id: 1, notes: "This is the first report", advance: 10 , selling_price: 200000000, business_sector: "Niaga", bank_id: 1, user_id: 1},
          {customer_id: 1, product_id: 2, report_capacity_id: 1, report_prospect_id: 1, notes: "This is the second report", advance: 15 , selling_price: 130000000, business_sector: "Importir", bank_id: 1, user_id: 2},
          {customer_id: 1, product_id: 3, report_capacity_id: 1, report_prospect_id: 2, notes: "This is the third report", advance: 25 , selling_price: 230000000, business_sector: "Niaga", bank_id: 1, user_id: 3},
          {customer_id: 1, product_id: 4, report_capacity_id: 1, report_prospect_id: 2, notes: "This is the fourth report", advance: 20 , selling_price: 330000000, business_sector: "Importir", bank_id: 2, user_id: 4},

          {customer_id: 2, product_id: 5, report_capacity_id: 1, report_prospect_id: 1, notes: "This is the first report Cust 2", advance: 10 , selling_price: 200000000, business_sector: "Niaga", bank_id: 1, user_id: 1},
          {customer_id: 2, product_id: 6, report_capacity_id: 1, report_prospect_id: 1, notes: "This is the second report Cust 2", advance: 15 , selling_price: 130000000, business_sector: "Perbangkan", bank_id: 1, user_id: 2},
          {customer_id: 2, product_id: 7, report_capacity_id: 1, report_prospect_id: 2, notes: "This is the third report Cust 2", advance: 25 , selling_price: 230000000, business_sector: "Exportir", bank_id: 1, user_id: 3},
          {customer_id: 2, product_id: 8, report_capacity_id: 1, report_prospect_id: 2, notes: "This is the fourth report Cust 2", advance: 20 , selling_price: 330000000, business_sector: "Bengkel", bank_id: 2, user_id: 4}
          ])

daily_report = DailyReport.create([
            {user_id: 1, activity_id: 1, report_id: 1}, 
            {user_id: 1, activity_id: 1, report_id: 2}, 
            {user_id: 1, activity_id: 1, report_id: 3}, 
            {user_id: 1, activity_id: 1, report_id: 4}, 
            {user_id: 2, activity_id: 2, report_id: 5},
            {user_id: 2, activity_id: 3, report_id: 6},
            {user_id: 2, activity_id: 2, report_id: 7},
            {user_id: 2, activity_id: 2, report_id: 8},
          ])
office_hours = OfficeHour.create([{start_hour: DateTime.strptime("08:00", "%H:%M"), end_hour: DateTime.strptime("10:00", "%H:%M")}, 
                                  {start_hour: DateTime.strptime("17:00", "%H:%M"), end_hour: DateTime.strptime("19:00", "%H:%M")}])